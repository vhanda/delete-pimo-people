#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

#include <Soprano/Statement>
#include <Soprano/Node>
#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/StatementIterator>
#include <Soprano/NodeIterator>
#include <Soprano/PluginManager>

#include <Nepomuk2/Resource>
#include <Nepomuk2/ResourceManager>
#include <Nepomuk2/Variant>
#include <Nepomuk2/File>
#include <Nepomuk2/Tag>

#include <Nepomuk2/Types/Property>
#include <Nepomuk2/Types/Class>

#include <Nepomuk2/Query/Query>
#include <Nepomuk2/Query/FileQuery>
#include <Nepomuk2/Query/ComparisonTerm>
#include <Nepomuk2/Query/LiteralTerm>
#include <Nepomuk2/Query/ResourceTerm>
#include <Nepomuk2/Query/QueryServiceClient>
#include <Nepomuk2/Query/ResourceTypeTerm>
#include <Nepomuk2/Query/QueryParser>
#include <Nepomuk2/Query/Result>

#include <KDebug>

// Vocabularies
#include <Soprano/Vocabulary/RDF>
#include <Soprano/Vocabulary/RDFS>
#include <Soprano/Vocabulary/NRL>
#include <Soprano/Vocabulary/NAO>

#include <Nepomuk2/Vocabulary/NIE>
#include <Nepomuk2/Vocabulary/NFO>
#include <Nepomuk2/Vocabulary/NMM>
#include <Nepomuk2/Vocabulary/NCO>
#include <Nepomuk2/Vocabulary/PIMO>
#include <Nepomuk2/Vocabulary/NCAL>

#include <KUrl>
#include <KJob>

#include <nepomuk2/datamanagement.h>
#include <nepomuk2/simpleresource.h>
#include <nepomuk2/simpleresourcegraph.h>
#include <nepomuk2/storeresourcesjob.h>

#include <iostream>

using namespace Soprano::Vocabulary;
using namespace Nepomuk2::Vocabulary;

class TestObject : public QObject {
    Q_OBJECT
public slots:
    void main();
    void newEntries(const QList<Nepomuk2::Query::Result>& list );
public:
    TestObject() {
        QTimer::singleShot( 0, this, SLOT(main()) );
    }

};

int main( int argc, char ** argv ) {

    KComponentData component( QByteArray("nepomuk-test") );
    QCoreApplication app( argc, argv );

    TestObject a;
    app.exec();
}

void TestObject::main()
{
    Nepomuk2::Query::ResourceTypeTerm type( PIMO::Person() );
    Nepomuk2::Query::Query q( type );

    Nepomuk2::Query::QueryServiceClient *client = new Nepomuk2::Query::QueryServiceClient(this);
    connect( client, SIGNAL(newEntries(QList<Nepomuk2::Query::Result>)),
             this, SLOT(newEntries(QList<Nepomuk2::Query::Result>)) );
    client->query( q );
}

void TestObject::newEntries(const QList< Nepomuk2::Query::Result >& list)
{
    foreach( const Nepomuk2::Query::Result &r, list ) {
        kDebug() << "Found " << r.resource();
        KJob * job = Nepomuk2::removeResources( QList<QUrl>() << r.resource().uri() );
        job->exec();
        kDebug() << "Deleted";
    }
}

#include "main.moc"